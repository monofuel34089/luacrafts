require("util")
local Keccak = require('./ethereum/keccak')

testSuite("keccak", function()
    doTest("create keccak256", function()
        local hash = Keccak:new256()
    end)

    doTest("write()", function()
        local hash = Keccak:new256()
        hash:write("abc")
    end)

    skipTest("write() where #data > rate")

    -- go-ethereium tests that "abc" digests to "4e03657aea45a94fc7d47ba826c8d667c0d1e6e33a64a036ec44f58fa12d6c45"
    skipTest("digest()", function()
        local hash = Keccak:new256()
        hash:write("abc")
        local digest = tohex(hash:digest())
        local expected = "4e03657aea45a94fc7d47ba826c8d667c0d1e6e33a64a036ec44f58fa12d6c45"
        assert.equal(digest, expected)
    end)


end)