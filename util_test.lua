local util = require('util')
local assert = require('mlib/assert')

testSuite("util", function()
    skipTest("strCmp()", function()
        -- TODO
    end)

    doTest("to bytes", function()
        assert.equal(intToBytes(5) , fromhex('05000000'))
        assert.equal(doubleToBytes(5.5), fromhex('0000000000001640'))
    end)

    doTest("uuid", function()
        local id = gen_uuid()
        assert.equal(string.len(id), 128 / 8)
    end)
end)