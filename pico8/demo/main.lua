debuglogfile = 'debug.txt'

function _init()
    -- reset debug output
    printh("INIT", debuglogfile, true)

    tStats = {}
    cpuStats = {}
    memStats = {}
    fpsStats = {}

end

-- function _update()
-- end

function _update60()
    debugStats()
end

function _draw()
    cls()
    drawChart(5,5,time(),'t',tStats)
    drawChart(28,5, 1, "cpu", cpuStats)
    drawChart(51,5, 512, "mem", memStats)

end

function debug_print(str)
    printh(str, debuglogfile)
end

function statUpdate(t,v)
    add(t, v, 1)
    if #t > 20 then
        deli(t)
    end
end

function debugStats()
    
    cur_t = time()
    statUpdate(tStats, cur_t)
    mem = stat(0)
    statUpdate(memStats, mem)
    cpu = stat(1)
    statUpdate(cpuStats, cpu)
    fps = stat(7)
    statUpdate(fpsStats, fps)

    updateStat = "t: " .. cur_t .. " mem: " .. mem .. " cpu: " .. cpu .. " fps: " .. fps
    debug_print(updateStat)
end

-- data should be an array of 0-20 elements
function drawChart(x,y, max, title, data)
    border = 7
    bg = 0
    fg = 8
    -- print(str, x, y)
    -- color()
    

    rect(x,y,x+21,y+11, border)
    for k, v in pairs(data) do
        x2 = 21 - k + x
        height = min(10, v / max * 10)
        y2 = y + (10 - height) + 1
        line(x2,y + 10, x2, y2,fg)
    end 
end