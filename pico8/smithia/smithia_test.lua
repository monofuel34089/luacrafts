local assert = require('mlib/assert')
require('pico8/test_stubs')

require("pico8/smithia/constants")
require("pico8/smithia/entities")
require("pico8/smithia/util")
require("pico8/smithia/world")
require("pico8/smithia/main")

testSuite('smithia', function()

    testSuite('main', function()
        doTest('init', function()
            _init()
        end)
    end)

    testSuite("util", function()
        doTest("draw_chart", function()
            data = {
                1,
                2,
                3,
                4,
                5,
            }
            draw_chart(0,0,5,'foo',data)
        end)
    end)
    
end)