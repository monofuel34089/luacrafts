
function new_entity(x,y,etype)
    -- x,y is in pixel space
    local e = {
        x=x,
        y=y,
        etype=etype,
        dy=0,
        dx=0,
        speed=0.2,
        airspeed = 0.1,
        gravity=0.08,
        jumpspeed=1.8,
        friction=0.2,
        clampx=1,
        clampy=3
    }
    function draw()
        dprint("draw " .. e.etype)
        spr(e.etype,e.x,e.y)
        dprint("x" .. e.x .. ' y ' .. e.y)
    end

    add(entities,e)
    add(drawFNs,draw)

    return e
end

function new_player(x,y)
    local e = new_entity(x,y,etype_player)
    function update()
        -- btn() to get state
        -- btnp() for a fresh button press
        -- btnp also resets every 15 frames by default
        -- 0-5 left right up down O X
        left = btn(0)
        right = btn(1)
        up = btn(2)
        down = btn(3)
        if down then
            -- TODO what should down do??
        end
        o = btn(4)
        if o then
            -- TODO
        end
        x = btn(5)
        if x then
            -- TODO
        end

        -- TODO apply physics
        map_x = e.x / 8
        map_y = e.y / 8
        map_flr_x = flr(map_x)
        map_flr_y = flr(map_y)
        map_ceil_x = ceil(map_x)
        map_ceil_y = ceil(map_y)
        walkable = isWalkable(map_flr_x,map_flr_y + 1) or isWalkable(map_ceil_x,map_flr_y + 1)
        if not walkable then
            -- gravity
            e.dy = e.dy + e.gravity
            if left then
                e.dx = e.dx-e.airspeed
            end
            if right then
                e.dx = e.dx + e.airspeed
            end
        else
            -- apply ground friction
            e.dx = e.dx * (1-e.friction)
            -- clamp above ground
            e.y = map_flr_y * 8
            -- negate gravity
            e.dy = 0

            -- jump only if we are on a walkable surface
            if up then
                e.dy = -e.jumpspeed
            end
            if left then
                e.dx = e.dx-e.speed
            end
            if right then
                e.dx = e.dx + e.speed
            end
            -- if jumping, apply a little extra oomph
            if up and left then
                e.dx = e.dx-e.speed
            end
            if up and right then
                e.dx = e.dx + e.speed
            end
        end

        -- clamp speed
        e.dy = min(e.dy,e.clampy)
        e.dy = max(e.dy,-e.clampy)
        e.dx = min(e.dx,e.clampx)
        e.dx = max(e.dx,-e.clampx)

        -- handle dy & dx
        e.x = e.x + e.dx
        e.y = e.y + e.dy
        -- clamp within screen for now
        if e.x < 0 then
            e.x = 0
        end
        if e.x > 8 * 16 then
            e.x = 8 * 15
        end
    end

    add(updateFNs,update)

    local xchart = {}
    local ychart = {}
    function drawxychart()
        add(xchart,e.dx + e.clampx,1)
        add(ychart,-e.dy + e.clampy,1)
        if #xchart > 20 then
            deli(xchart)
        end
        if #ychart > 20 then
            deli(ychart)
        end
        draw_chart(5,5, e.clampx * 2, 'dx', xchart)
        draw_chart(28,5, e.clampy * 2, 'dy', ychart)
        
    end
    add(drawFNs, drawxychart)
end