local io = require("io")
require("./util")
require("mlang/tokenizer")

-- TODO should probably be recursive for function blocks
function generate_ast(input) 
    block = {
        type = "block",
        contents = {}
    }

    tokens = {}
    for token in lang_tokenize(input) do
        table.insert(tokens,token)
    end


    local next = function() return table.remove(tokens, 1) end
    local peek = function() return tokens[1] end


    for token in next do
        function token_err() 
            error("unhandled token: " .. toPrettyPrint(token))
        end

        function pop_expression()
            local expression = {
                type = "expression",
            }

            local token1 = next()
            local token2 = peek()

            if token2.type == 'punctuation' and token2.value == "(" then
                -- this is a function call
                next() -- pop the peek()ed token
            else
                error("confused on current expression" .. toPrettyPrint(token1))
            end


            return expression
        end

        if token.type == "keyword" then
            if token.value == 'local' then
                local var_name = next()
                if (var_name.type ~= "keyword") then
                    error("expected a string to follow 'local'. got "  .. toPrettyPrint(var_name))
                end
                local operator = next()
                if (operator.type ~= 'operator' or operator.value ~= '=') then
                    error("expected an operator: ".. toPrettyPrint(operator))
                end

                expression = pop_expression();


                local statement = {
                    type = "statement",
                    isLocal = true,
                    variable = var_name.value,
                    operation = operator.value,
                    expression = expression,
                }
                -- TODO handle nested statements
                table.insert(block.contents, statement)

            else
                token_err()
            end
            
        else
            token_err()
        end
    end
    return block
end

local Lang = {}

function Lang:new(filepath)
    local lang = setmetatable({}, { __index = Lang })

    file = io.open(filepath, "r")

    lang.file = file
    lang.block = {}
  
    -- for c in file:lines() do
    --   print(c)
    -- end

    lang.block = generate_ast(file)

    return lang
end

function Lang:unroll()
    -- TODO
    -- TODO should probably be immutable, return a copy
    -- scan through the syntax tree for `require` and unroll them
    -- TODO optional skip certain requires?


end


-- when testing mlang on itself, this function is to test various edge cases
function edgeCases()
    local a, b

    a = 5; b = 
    2 a=3
end

return Lang
