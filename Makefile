LUA=lua5.3
LUAROCKS=luarocks

.PHONY: setup
setup:
	$(LUAROCKS) install luasec
	$(LUAROCKS) install luasocket
	$(LUAROCKS) install luacheck
	$(LUAROCKS) install --server=https://luarocks.org/dev lua-nums
	$(LUAROCKS) install luacov-coveralls --server=http://rocks.moonscript.org/dev

.PHONY: check
check:
	luacheck .

.PHONY: test
test:
	$(LUA) test.lua

.PHONY: coverage
coverage:
	$(LUA) -lluacov test.lua
	luacov-coveralls || true

# $(LUA) test.lua
# TODO migrate this to match other tests
.PHONY: testKV
.testKV:
	cat kvstore/test_scripts/test_populate.txt | $(LUA) kvstore/repl.lua --memory