local assert = require('mlib/assert')
local Hashmap = require("mlib/hashmap")

testSuite("hashmap", function() 

    doTest("create hashmap", function()
        local map = Hashmap:new()
        assert.equal(#map, 0)
    end)

    doTest("add to hashmap", function()
        local map = Hashmap:new()
        map['A'] = 1
        assert.equal(#map, 1)
        map['B'] = 2
        map['C'] = 3
        assert.equal(#map, 3)

        assert.equal(map['A'], 1)
        assert.equal(map['B'], 2)
        assert.equal(map['C'], 3)
    end)

    doTest("delete from hashmap", function()
        local map = Hashmap:new()
        map['A'] = 1
        map['B'] = 2
        map['C'] = 3

        map['B'] = nil
        assert.equal(#map, 2)
        assert.equal(map['B'], nil)
        map['C'] = nil
        map['A'] = nil
        assert.equal(#map, 0)
    end)

    skipTest("keys()")
    skipTest("values()")
    skipTest("entries()")
end)