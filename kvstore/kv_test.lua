require('./util')
require('./encoder')
-- require('./schema')
require('./kvstore/wal')
require('./kvstore/kv')
local assert = require('mlib/assert')

testSuite("kvstore", function()
    testSuite("kv schemas", function()
        doTest("WAL schema", function()
            local store = {
                tables = {
                    {
                        name = 'default',
                        entries = {
                            {
                                key = 'foo',
                                value = 'bar'
                            },
                            {
                                key = 'foo2',
                                value = 5
                            },
                            {
                                key = 'foo3',
                                value = 5.44
                            }
                        }
                    }
                }
            }
            local store_buf = encode(store, KV_Schema)
            local store_dec = decode(store_buf, KV_Schema)
            assert.equal(store_dec.tables[1].name, 'default')
            assert.equal(#store_dec.tables[1].entries, 3)
        end)

        doTest("WAL events", function()
            local wal_noop = {
                ID = 1,
                kind = WAL_Kinds.noop,
                bytes = ""
            }
            local wal_buf = encode(wal_noop, WAL_Schema)
            logInfo('WAL ' .. tohex(wal_buf))
            local wal_ev = decode(wal_buf, WAL_Schema)
            assert.equal(wal_ev.ID, wal_noop.ID)
            assert.equal(wal_ev.kind, 0)
            assert.equal(wal_ev.bytes, "")
            handle_wal_event(wal_ev)
        end)
    end)

    testSuite("db", function()

        doTest("tokenizer", function()
            local res = ''
            res = tokenize("PING")
            assert.equal(res, { "PING" })
            res = tokenize('PING "hello world"')
            assert.equal(res, { "PING", "\"hello world\"" })
            res = tokenize('GET foobar')
            assert.equal(res, { "GET", "foobar" })
        
            res = tokenize('SET foo bar')
            assert.equal(res, { "SET", "foo", "bar" })
            res = tokenize('SET foo "hello world"')
            assert.equal(res, { "SET", "foo", "\"hello world\"" })
        end)

        doTest("WAL tests", function()

            local db_name = 'test'
            local dir = './kvstore/test_db'
            local wal_filepath = dir .. '/' .. db_name .. '.wal'
            -- local db_filepath = dir .. '/' .. db_name .. '.bin'
        
        
            local wal = {
                {
                    ID = 1,
                    kind = WAL_Kinds.noop,
                    bytes = ""
                },
                {
        
                    ID = 2,
                    kind = WAL_Kinds.create,
                    bytes = ""
                }
            }
        
            local wal_buf = ""
            for k, v in ipairs(wal) do
                local buf = encode(v, WAL_Schema)
                wal_buf = wal_buf .. buf
            end
        
            local wal_file = io.open(wal_filepath, "wb")
            -- local db_file = io.open(db_filepath, "wb")
        
            logInfo(tohex(wal_buf))
            wal_file:write(wal_buf)
            -- NB. written out wal file is used in exec test
            -- TODO should de-couple these tests
        end)

        doTest("exec", function()
            local store = KV_Store:new({ name = 'test1', reset = true})
            local res = ''
            res = store:exec("PING")
            assert.equal(res, "PONG")
        
            res = store:exec('PING "hello world"')
            assert.equal(res, '"hello world"')
        
            res = store:exec("GET foo")
            assert.equal(res, nil)
        
            res = store:exec("SET foo \"hello world\"")
            assert.equal(res, "SET foo")
        
            res = store:exec("GET foo")
            assert.equal(res, "\"hello world\"")
        
            res = store:exec("DELETE foo")
            assert.equal(res, "DEL foo")
        
            res = store:exec("GET foo")
            assert.equal(res, nil)
        
            res = store:exec("SET foo2 \"hello world!\"")
            assert.equal(res, "SET foo2")
        
            -- TODO test that it's stored internally as binary
            res = store:exec("SET hex 0x123456")
            res = store:exec("GET hex")
            assert.equal(res, "0x123456")
        
            store:listen()
            store:tick()
        
            store:flush()
            store:close()
        
            -- load DB back in
            local store2 = KV_Store:new({ name = 'test1', reset = false })
            res = store2:exec("GET foo2")
            assert.equal(res, "\"hello world!\"")
            store2:close()

            -- -- TODO test WAL when saving/loading
            -- local store3 = KV_Store:new({ name = 'test3', reset = true })
            -- store3.exec('SET foo3 "Hello World"' )
            -- -- foo
            -- local value_buf = encode({
            --     key = 'foo3',
            --     value = encode('Hello World 2'),
            --     table = 'default'
            -- }, WAL_Schemas[set])
            -- local buf = encode({
            --     ID = 2,
            --     kind = 'set',
            --     bytes = value_buf
            -- }, WAL_Schema)
            -- store.wal_file:write(buf)
            -- store.wal_file:flush()

            -- TODO test loading
            -- stuff
        end)

        -- TODO need a good way to clean up stuff if a test fails
        doTest("networking", function()
            local leader = KV_Store:new({
                name = 'leader',
                in_memory = true,
                reset = true,
                leader_port = 25600,
                role = 'leader',
                repl_port = 25601
            })
            local follower = KV_Store:new({
                name = 'follower',
                in_memory = true,
                reset = true,
                leader_port = 25600,
                role = 'follower',
                repl_port = 25602,
                leader_host = 'localhost'
            })
        
            leader:listen()
            follower:listen()
            follower:follow()
            
            logDebug('resuming coroutines')
            leader:tick()
            follower:tick()
                
            -- sockets should be [leader_server, repl_server, client]
            local followerCount = leader.net:countFollowers()
            if followerCount ~= 1 then
                error("expected client to connect. # of db_clients should be 1: " .. followerCount)
            end
        
            if isOC() then
                -- TODO OpenComputers Networking
                logWarn('repl test not implemented for open computers')
            else
                local socket = require('socket')
        
                -- connect to leader REPL
                local client, err = socket.connect('localhost', 25601)
                if err then
                    error('failed to connect to leader REPL: ' .. err)
                end
                client:settimeout(1)
                
                logDebug('connected to leader REPL')
                leader:tick()
                client:send('PING\n')
                
                leader:tick()
                local line, err = client:receive('*l')
                if err then
                    error('client receive error: ' .. err)
                end
                assert.equal(line, "PONG")
        
                client:send('SET FOO 0x123456\n')
                leader:tick()
                follower:tick()
                local line, err = client:receive('*l')
                if err then
                    error('client receive error: ' .. err)
                end
                assert.equal(line, "SET FOO")
        
                client:send('GET FOO\n')
                leader:tick()
                local line, err = client:receive('*l')
                if err then
                    error('client receive error: ' .. err)
                end
                assert.equal(line, "0x123456")
                client:close()
        
                -- connect to follower REPL
                local client, err = socket.connect('localhost', 25602)
                if err then
                    error('failed to connect to follower REPL: ' .. err)
                end
                client:settimeout(1)
                
                logDebug('connected to follower REPL')
                follower:tick()
                client:send('PING\n')
                
                follower:tick()
                local line, err = client:receive('*l')
                if err then
                    error('client receive error: ' .. err)
                end
                assert.equal(line, "PONG")
        
                client:send('GET FOO\n')
                follower:tick()
                local line, err = client:receive('*l')
                if err then
                    error('client receive error: ' .. err)
                end
                assert.equal(line, "0x123456")
                logDebug("got " .. line .. " from follower!")
        
                client:close()
            end
        
            leader:close()
            follower:close()
        end)
    end)
end)