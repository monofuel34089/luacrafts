require('./kvstore/config')

-- TODO should use mlog instead

function logDebug(str)
    log('DEBUG', str)
end

function logInfo(str)
    log('INFO', str)
end

function logErr(str)
    log('ERR', str)
end

function logWarn(str)
    log('WARN', str)
end

-- levels
-- ERR WARN INFO DEBUG
function log(level, str)
    -- TODO use LOG_LEVEL
    print(level, "|", str)
end

