require("mfs/fs")
local assert = require('mlib/assert')

testSuite("fs", function()

    doTest("ls()", function()
        -- TODO shouldn't assume on sorted output
        local res = ls("./mfs")
        local expected = {
            "./mfs",
            "./mfs/fs_test.lua",
            "./mfs/fs.lua",
        }
        assert.equal(res, expected)
    end)

    doTest("dir() on file", function()
        local res = isDir("./mfs/fs.lua")
        assert.equal(res, false)
    end)

    doTest("dir() on dir", function()
        local res = isDir("./mfs/")
        assert.equal(res, true)
    end)

end)